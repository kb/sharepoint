SharePoint API App
******************

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-sharepoint
  # or
  python3 -m venv venv-sharepoint
  source venv-sharepoint/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
