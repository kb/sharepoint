# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    help = "Initialise SharePoint app"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        self.stdout.write("{} - Complete".format(self.help))
