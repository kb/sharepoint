source venv-sharepoint/bin/activate.fish
set -x DATABASE_HOST ""
set -x DATABASE_PASS ""
set -x DATABASE_USER ""
set -x DEFAULT_FROM_EMAIL "web@pkimber.net"
set -x DJANGO_SETTINGS_MODULE "example_sharepoint.dev_patrick"
set -x MAIL_TEMPLATE_TYPE "django"
set -x SECRET_KEY "the_secret_key"
source .private
