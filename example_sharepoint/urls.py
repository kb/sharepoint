# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path

from .views import DashView, HomeView, SettingsView


admin.autodiscover()


urlpatterns = [
    path("admin/", admin.site.urls),
    url(regex=r"^", view=include("login.urls")),
    url(regex=r"^$", view=HomeView.as_view(), name="project.home"),
    url(regex=r"^dash/$", view=DashView.as_view(), name="project.dash"),
    url(regex=r"^mail/", view=include("mail.urls")),
    url(
        regex=r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        url(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
